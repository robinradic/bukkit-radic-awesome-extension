package org.radic.extension.awesome.sysinfo;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.radic.core.RadicCore;

public class AlarmThread implements Runnable {
    private int interval = 60;
    private int triggerValue = 90;
    private boolean automaticGC = false;

    public void init() {
        try{
            interval = RadicCore.sysinfo.getInt("ram-check-interval");
            triggerValue = RadicCore.sysinfo.getInt("ram-alarm-level");
            automaticGC = RadicCore.sysinfo.getBoolean("auto-gc");
        }catch(Exception e){
          //  RadicCore.log(RadicCore.locale.getString("log.config-error"));
        }
    }

    @Override
    public void run() {
        while(true){
            int rambefore = InfoGatherer.getRawRamUsage()[0];
            if (InfoGatherer.getRawRamUsage()[0] >= triggerValue){
                sendMessages();
                if(automaticGC) {
                   // RadicCore.collectGarbage(RadicCore.server.getConsoleSender());
                    for(Player p : RadicCore.server.getOnlinePlayers()){
                        if(p.hasPermission("RadicCore.alarm-recv")){
                            RadicCore.msgPlayer(p.getName(), RadicCore.locale.getString("ui.alarm.ram-auto-clearing"));
                            String s = RadicCore.locale.getString("ui.alarm.ram-auto-cleared");
                            s = s.replaceAll("@before", rambefore+"");
                            s = s.replaceAll("@after", InfoGatherer.getRawRamUsage()[0]+"");
                        }
                    }
                }
            }
            try {
                Thread.sleep(interval*1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void sendMessages() {

        String fullmsg = RadicCore.locale.getString("alert-msg-prefix");
        String noPrefixMsg = RadicCore.locale.getString("alert-msg-ram");

        noPrefixMsg = noPrefixMsg.replaceAll("@ram", InfoGatherer.getRawRamUsage()[0]+"");
        noPrefixMsg = noPrefixMsg.replaceAll("@crit-ram", triggerValue+"");

        fullmsg = fullmsg.replaceAll("@msg", noPrefixMsg);
        fullmsg = ChatColor.translateAlternateColorCodes('&', fullmsg);
        for (Player p : RadicCore.server.getOnlinePlayers()) {
            if (p.hasPermission("RadicCore.alarm-recv")) {
                p.sendMessage(fullmsg);
            }
        }
        RadicCore.server.getConsoleSender().sendMessage(fullmsg);

    }

}