package org.radic.extension.awesome.sysinfo;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.radic.core.commands.CommandHandler;
import org.radic.core.utils.Configuration;
import org.radic.extension.awesome.sysinfo.InfoGatherer;

import java.io.File;

/**
 * Created by radic on 7/20/14.
 */
public class RadicSysinfo {

    public static JavaPlugin plugin;
    public static Server server;

    public static Configuration configuration;
    public static FileConfiguration config;
    public static FileConfiguration sysinfo;
    public static FileConfiguration locale;

    public static File dataFolder;

    public static CommandHandler commands;


    public static void setConfiguration(Configuration configuration)
    {
        RadicCore.configuration = configuration;
        RadicCore.config = configuration.getConfig();
        RadicCore.sysinfo = configuration.getSysinfo();
        RadicCore.locale = configuration.getLocale();
    }

    public static void message(String name, String value, CommandSender sender)
    {
        sender.sendMessage(ChatColor.GOLD + "[ServerInfo " + ChatColor.GREEN + name + ChatColor.GOLD + "] " + ChatColor.WHITE + value);
    }

    public static void msgPlayer(String playername, String msg){
        Player p = RadicCore.server.getPlayerExact(playername);
        p.sendMessage(msg);
    }


    public static void msgSysinfo(CommandSender sender) {

        senderMSG(locale.getString("sys-arch"), InfoGatherer.getInfo(InfoGatherer.infoType.SYSTEM_ARCH), sender);

        // system name and version
        senderMSG(locale.getString("sys-os"),
                        InfoGatherer.getInfo(InfoGatherer.infoType.SYSTEM_NAME) + " "
                        + locale.getString("sys-os-version") + " "
                        + InfoGatherer.getInfo(InfoGatherer.infoType.SYSTEM_VERSION), sender);

        // bukkit version
        senderMSG(locale.getString("bukkit-version"), InfoGatherer.getInfo(InfoGatherer.infoType.BUKKIT_VERSION), sender);

        // players x of max y
        senderMSG(locale.getString("players"),InfoGatherer.getInfo(InfoGatherer.infoType.PLAYERS_NOW) + " "
                        + locale.getString("players-of-max") + " "
                        + InfoGatherer.getInfo(InfoGatherer.infoType.PLAYERS_MAX), sender);

        // server ip:port
        senderMSG(locale.getString("server-ip"), InfoGatherer.getInfo(InfoGatherer.infoType.SERVER_IP) + ":"
                        + InfoGatherer.getInfo(InfoGatherer.infoType.SERVER_PORT), sender);

        // server motd
        senderMSG(locale.getString("motd"), InfoGatherer.getInfo(InfoGatherer.infoType.SERVER_EXTERN_MOTD), sender);

        // temp

        String temp = ChatColor.translateAlternateColorCodes('&', "&6" + locale.getString("temp") + ":&c "
                + InfoGatherer.getInfo(InfoGatherer.infoType.MACHINE_TEMP));

        senderMSG(locale.getString("temp"), temp, sender);

        // ram usage
        String ramUsage = ChatColor.translateAlternateColorCodes('&',"&6" + locale.getString("ram-usage") + ":&c "+ InfoGatherer.getInfo(InfoGatherer.infoType.MACHINE_RAM_USAGE));
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ramUsage));

        // ram bar [#####000000]
        String ramBar = ChatColor.translateAlternateColorCodes('&',"&6" + locale.getString("ram-bar") + ":&c "+ InfoGatherer.getInfo(InfoGatherer.infoType.MACHINE_RAM_GRAPH));
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ramBar));

    }

    private static void senderMSG(String title, String msg, CommandSender sender) {
        message(title, msg, sender);


    }


}
