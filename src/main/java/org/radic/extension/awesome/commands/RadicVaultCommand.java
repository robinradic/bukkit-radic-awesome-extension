package org.radic.extension.awesome.commands;
//Imports for the base command class.

import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.radic.core.Radic;
import org.radic.core.commands.CommandInterface;
import org.radic.extension.awesome.RadicAwesomeExtension;

//This class implements the Command Interface.
public class RadicVaultCommand implements CommandInterface
{


    private void msg(String name, String message){
        Radic.console("radic-awesome:vault", name, message);
    }

    private static Object eco(){
        return RadicAwesomeExtension.econ;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args, JavaPlugin plugin) {
        boolean enabled = RadicAwesomeExtension.econ.isEnabled();
        msg("econ:is-enabled", enabled ? "yes" : "no");
        if(enabled){
            boolean bankSupport = RadicAwesomeExtension.econ.hasBankSupport();
            msg("econ:has-bank-support", bankSupport ? "yes" : "no");
            if(bankSupport) {
                msg("offline-players:toostring", plugin.getServer().getOfflinePlayers().toString());
                for (OfflinePlayer player : plugin.getServer().getOfflinePlayers()) {
                    msg("offline-player:player:name", player.getName());


                    EconomyResponse response = RadicAwesomeExtension.econ.depositPlayer(player, 44);
                    msg("economy:depositPlayer:economy-response:amount", response.amount + "");
                    msg("economy:depositPlayer:economy-response:amount", response.balance + "");
                    msg("economy:depositPlayer:economy-response:responsetype", response.type.name() + "");


                    response = RadicAwesomeExtension.econ.createBank(player.getName() + "HisBank", player);
                    msg("economy:createbank:economy-response:amount", response.amount + "");
                    msg("economy:createbank:economy-response:amount", response.balance + "");
                    msg("economy:createbank:economy-response:responsetype", response.type.name() + "");
                }
            }
        }
        Radic.console("radic:command", "awesome");
        return true;
    }
}